package com.sourceit.phonecontact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int PERMISSION_REQUEST_CONTACT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (hasPermission()) {
            readContacts();
        } else {
            askPermission();
        }
    }

    private void askPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_CONTACTS},
                PERMISSION_REQUEST_CONTACT);
    }

    private boolean hasPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CONTACT &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            readContacts();
        }
    }

    public static final String SPACE = " ";

    private void readContacts() {
        // get ContentResolver
        ContentResolver cr = getContentResolver();
        //create query
        Cursor cur = cr.query(Contacts.CONTENT_URI,
                new String[] {Contacts._ID, Contacts.DISPLAY_NAME, Contacts.HAS_PHONE_NUMBER},
                null,
                null,
                Contacts._ID + SPACE + "ASC");

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(0);
                String name = cur.getString(1);
                Log.i("userName", id + " " + name);

                if (cur.getInt(2) > 0) {
                    getPhoneNumbers(id);
                }
            }
        }
        //don’t forget
        cur.close();
    }

    private List<String> getPhoneNumbers(String id) {
        List<String> list = new ArrayList<>();
        Cursor pCur = getContentResolver().query(
                Phone.CONTENT_URI, // table name
                new String[]{Phone.NUMBER}, // select only numbers
                Phone.CONTACT_ID + " = ?", // where contact id == user id
                new String[]{id}, // user id
                null); // without sorting
        while (pCur.moveToNext()) {
            String phoneNo = pCur.getString(0);
            list.add(phoneNo);
            Log.i("userName", phoneNo);
        }
        pCur.close();
        return list;
    }
}